var utils = require('./lib/utils.js');
var server = require('./lib/rest.js');
var pkg = require('./package.json');

server.listen(8080, function() {
  utils.log('--- %s version %s listening at %s', server.name, pkg.version, server.url);
});
