## Use your Tessel2 + relay module as a Wi-Fi KNX actuator.

- Directly talks to your KNX router via UDP multicast, no eibd or special arrangement required.
- Exposes a simple REST api (via restify) that you can use to configure group addresses.
- Persists configuration to /app/storage so that you don't need to reconfigure after a reboot or power outage.

[See also: Hackster.io project](https://www.hackster.io/ekarak/tessel-2-knx-wi-fi-actuator-28010f)

![Tessel 2](https://hackster.imgix.net/uploads/attachments/269843/lenovo_a1000_img_20170304_111638_QCXvIX2wbV.jpg?auto=compress%2Cformat&w=900&h=675&fit=min)

### How do I get set up? ###

* Summary of set up
```
$ t2 root
$ t2 push index.js
```

### REST assured: configuring your KNX actuator doesn't require ETS anymore :)

JUC: Just Use Curl to configure the actuator (assuming your device is named `TesselKNX`).  Your KNX/IP router should be on the same LAN and have multicast enabled.  That's all there is to it !

```sh
# get current relay status
$ curl -s  http://TesselKNX.local:8080/relays
{"1":{"state":false,"groupAddrs":{"control":[],"status":[]}},"2":{"state":false,"groupAddrs":{"control":[],"status":[]}}}

# bind control group address 5/0/8 to relay 1
$ curl -s -X PUT http://TesselKNX.local:8080/relay/1/control/5/0/8
{"result":"OK","relays":{"1":{"state":false,"groupAddrs":{"control":["5/0/8"],"status":[]}},"2":{"state":false,"groupAddrs":{"control":[],"status":[]}}}}

# bind another control group address to relay 1
$ curl -s -X PUT http://TesselKNX.local:8080/relay/1/control/5/0/80
{"result":"OK","relays":{"1":{"state":false,"groupAddrs":{"control":["5/0/8","5/0/80"],"status":[]}},"2":{"state":false,"groupAddrs":{"control":[],"status":[]}}}}

# bind status group address 2/3/4 to relay 1
$ curl -s -X PUT http://TesselKNX.local:8080/relay/1/status/2/3/4
{"result":"OK","relays":{"1":{"state":false,"groupAddrs":{"control":["5/0/8","5/0/80"],"status":["2/3/4"]}},"2":{"state":false,"groupAddrs":{"control":[],"status":[]}}}}

# delete all control group addresses for relay 1
$ curl -s -X DELETE http://TesselKNX.local:8080/relay/1/control
{"result":"OK","relays":{"1":{"state":false,"groupAddrs":{"control":[],"status":["2/3/4"]}},"2":{"state":false,"groupAddrs":{"control":[],"status":[]}}}}
```

If you need to use unicast ("IP tunnelling" in KNX terminology) to connect to your KNX/IP router, then you need to override the default configuration with a POST to `/config` as follows:

```sh
# curl -s -X POST -d '{"knxrouter": { "ipAddr": "192.168.2.1", "ipPort": 3671 } }' http://TesselKNX.local:8080/config
# get current configuration
# curl -s http://TesselKNX.local:8080/config
```


**DISCLAIMER: This is an experimental open-source project.** As such, *do not*, repeat **do not** use it for real-world scenarios, including, but not limited to controlling real loads. You really don't want to fry your equipment or set your flat on fire. You can't say I didn't warn you!