var restify = require('restify');
var plugins = require('restify-plugins');

var relayctl = require('./relayctl');
var knxconn = require('./knxconn');
var storage = require('./storage');
var utils = require('./utils');

var pkg = require('../package.json');

// ==============
// REST interface
// ==============
const server = restify.createServer({name: pkg.name, version: pkg.version});
server.use(plugins.acceptParser(server.acceptable));
server.use(plugins.queryParser());
server.use(plugins.bodyParser());

// ========== GLOBAL CONFIGURATION ============
/*
 GET /config ==> JSON config array
*/
server.get('/config', function(req, res, next) {
  utils.log('GET /config');
  res.json(storage.config);
  return next();
});

/*
* POST /config <json body>
* override json config
*/
server.post('/config', function(req, res, next) {
  utils.log('POST config: %s', req.body);
  config = JSON.parse(req.body);
  storage.setItem('config', config);
  res.send({'result': 'OK', 'config': config});
  return next();
});


// ========== RELAY GROUP ADDRESSES ==========
/*
 GET /relays ==> relays JSON control+status array
{"1":{"state":false,"groupAddrs":{"control":["5/0/8"],"status":[]}},"2":{"state":false,"groupAddrs":{"control":[],"status":[]}}}
*/
server.get('/relays', function(req, res, next) {
  utils.log('GET /relays');
  res.json(relayctl.relays);
  return next();
});

/*
* PUT /relay/1/control/1/2/3 => append 1/2/3 in the list of group addresses controlling relays[1]
* PUT /relay/2/status/4/5/6 => set relays[2].status group address to 4/5/6
* Use this to define group addresses for each relay channel.
* Note: there's only ONE status group address per relay channel
*/
server.put(/\/relay\/([0-9]+)\/(control|status)\/(.*)/, function(req, res, next) {
  var channel   = req.params[0];
  var gaType    = req.params[1];
  var groupAddr = req.params[2];
  utils.log('PUT channel = %d, %s ga = %s.', channel,gaType, groupAddr);
  // todo: validate groupAddr is a valid KNX address
  knxconn.setupDatapoint(channel, gaType, groupAddr);
  storage.setItem('relays', relayctl.relays);
  res.send({'result': 'OK', 'relays': relayctl.relays});
  return next();
});

/*
DELETE /relay/1/control : delete ALL control datapoints for relay 1
DELETE /relay/2/status : delete status datapoint for relay 2
*/
server.del(/\/relay\/([0-9]+)\/(control|status)/, function(req, res, next) {
  var channel   = req.params[0];
  var gaType    = req.params[1];
  utils.log('DELETE channel = %d, %s', channel, gaType);
  knxconn.teardownDatapoints(channel, gaType);
  storage.setItem('relays', relayctl.relays);
  res.send({'result': 'OK', 'relays': relayctl.relays});
});

module.exports = server;
