var knx    = require('knx');
var util   = require('util');
var tessel = require('tessel');

var utils   = require('./utils');
var storage = require('./storage');
var relayctl  = require('./relayctl');

// "<channel>.(control|status)" => <knx.Datapoint>
// e.g. "1.control" => ...
var datapoints = {};

// ==============
// KNX connection
// ==============
var connection = new knx.Connection(storage.config.knxrouter);
connection.on('event', function (evt, src, dest, value) {
  utils.log("**** KNX EVENT: %j, src: %j, dest: %j, value: %j",
    evt, src, dest, value);
  utils.flash(utils.leds.green); // flash the green LED on KNX activity
});

module.exports = {

  connection: connection,

  setupDatapoint: function (channel, gaType, groupAddr) {
    var channel = parseInt(channel);
    utils.log('setupDatapoint(%d, %s, %s)', channel, gaType, groupAddr);
    var key = util.format("%d.%s", channel, gaType);
    var datapoint = new knx.Datapoint({ga: groupAddr, dpt: '1'}, connection);
    switch (gaType) {
      case 'control':
        datapoint.on('change', function(oldval, newval) {
          utils.log('control datapoint change %s: %s (%s)', groupAddr, newval, typeof newval);
          relayctl.setValue(channel, newval)
        });
        if (relayctl.relays[channel].groupAddrs[gaType].indexOf(groupAddr) == -1) {
          // push new GA to groupAddrs subarray
          relayctl.relays[channel].groupAddrs[gaType].push(groupAddr);
        }
        // also store the datapoints, so that we can unbind the event handlers upon DELETE
        if (datapoints.hasOwnProperty(key)) {
          datapoints[key].push(datapoint);
        } else {
          datapoints[key] = [datapoint];
        }
        break;
      case 'status':
        // notify KNX when the relay latch status changes
        relayctl.latchWatchers[channel] = function(value) {
          utils.log('status datapoint change %s: %s', groupAddr, value);
          datapoint.write(value);
        }
        // there's only ONE status GA per relay
        relayctl.relays[channel].groupAddrs.status = [groupAddr];
        datapoints[key] = [datapoint];
        break;
    }
    utils.log('setupDatapoint: datapoints = %j', datapoints);
  },

  teardownDatapoints: function (channel, gaType) {
    channel = parseInt(channel);
    utils.log('teardownDatapoints(%d, %s)', channel, gaType);
    var key = util.format("%d.%s", channel, gaType);
    for (var i in datapoints[key]) {
      datapoints[key][i].removeAllListeners('change');
    }
    relayctl.relays[channel].groupAddrs[gaType] = [];
    if (gaType == 'status') delete relayctl.latchWatchers[channel];
  }
}

// restore KNX datapoints upon boot-up
Object.keys(relayctl.relays).forEach(function(channel) {
  if (relayctl.relays[channel].groupAddrs) {
    Object.keys(relayctl.relays[channel].groupAddrs).forEach(function(gaType) {
      for (var gaIdx in relayctl.relays[channel].groupAddrs[gaType]) {
        var ga = relayctl.relays[channel].groupAddrs[gaType][gaIdx];
        module.exports.setupDatapoint(channel, gaType, ga);
      }
    })
  }
})
