// ==============
// RELAYS
// ==============
var tessel  = require("tessel");
var EventEmitter = require('events');
var relay   = tessel.port ? require('relay-mono').use(tessel.port['A']) :  new EventEmitter();
var utils   = require('./utils');
var storage = require("./storage");

/* relay_id => {
  state: boolean,
  groupAddrs: {
    control: [<group address>, <group address>, ...],
    status: [<group address>]
  }
  TODO: on_delay, off_delay, etc etc
} */
var relays = storage.getItemSync('relays') || {
  1: {state: false, groupAddrs: { control: [], status: [] }},
  2: {state: false, groupAddrs: { control: [], status: [] }}
};
// channel => closure
var latchWatchers = {};

relays.moduleReady = false;

// Wait for the relay module to connect
relay.on('ready', function () {
  utils.log("**** RELAYS READY!");
  relays.moduleReady = true;
});

relay.on('latch', function(channel, value) {
  utils.log('relay %s latch change: %j', channel, value);
  relays[channel].state = value;
  utils.flash(utils.leds.blue);
  if (typeof latchWatchers[channel] == 'function') {
    latchWatchers[channel](value);
  }
});

module.exports = {
  relays: relays,
  latchWatchers: latchWatchers,
  setValue: function(channel, value) {
    if (relays.moduleReady) relay._setValue(channel, value);
  }
}
