var util   = require('util');
var tessel = require('tessel');

module.exports = {
  leds: {
    // an array of available LEDs
    red:   tessel.led && tessel.led[0],  // ERR - Red
    amber: tessel.led && tessel.led[1], // WLAN - Amber
    green: tessel.led && tessel.led[2], // LED0 - Green
    blue:  tessel.led && tessel.led[3], // LED1 - Blue
  },

  flash: function (led) {
    if (!led) return;
    led.on();
    setTimeout(function() {
      led.off();
    }, 100);
  },

  log: function (fmt) {
    var msg = arguments.length > 1 ?
      util.format.apply(util, Array.prototype.slice.apply(arguments)) :
      fmt;
    console.log("%s: %s",
      new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), msg
    );
  }
}
