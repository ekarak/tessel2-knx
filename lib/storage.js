// init persistent storage
var tessel  = require("tessel");
var persist = require('node-persist');
var storage = persist.create({dir: tessel.port ? '/app/storage':'/tmp/storage'});
storage.initSync();

module.exports = {
  setItem:     function(key, val) { storage.setItem(key, val); },
  getItemSync: function(key)      { return storage.getItemSync(key); },
  config: storage.getItemSync('config') || {
     knxrouter: {
       ipAddr: '224.0.23.12', // ip address of the KNX router or interface
       ipPort: 3671,          // the UDP port of the router or interface
       physAddr: '15.15.15',  // the KNX physical address we want to use
       minimumDelay: 10,      // wait at least 10 millisec between each datagram
       //debug: true
     }
  },
}
